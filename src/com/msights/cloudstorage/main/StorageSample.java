package com.msights.cloudstorage.main;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.StorageScopes;
import com.google.api.services.storage.model.Bucket;
import com.google.api.services.storage.model.StorageObject;

/**
 * Main class for the Cloud Storage API command line sample.
 * Demonstrates how to make an authenticated API call using OAuth 2 helper classes.
 */
public class StorageSample {
	
	/**
	   * Be sure to specify the name of your application. If the application name is {@code null} or
	   * blank, the application will log a warning. Suggested format is "MyCompany-ProductName/1.0".
	   * If you are running the sample on a machine where you have access to a browser, set 
	   * AUTH_LOCAL_WEBSERVER to true.
	   */
	  private static final String APPLICATION_NAME = "API_Project";
	  //private static final String BUCKET_NAME = "dfa_-44b573f0bd12fb8c5abf8304abac529f5d453da5";	// NA (5823)
	  private static final String BUCKET_NAME = "dfa_-7e5cc8e39a2f4c8c07dba54f7331727386b34b55";	// EMEA (5851)
	  private static final String CLIENT_SECRET_FILENAME = "conf/hp_client_secrets.json";
	  
	  /** Directory to store user credentials. */
	  private static final java.io.File DATA_STORE_DIR = new java.io.File("store/hp_storage_sample");
	  
	  /**
	   * Global instance of the {@link DataStoreFactory}. The best practice is to make it a single
	   * globally shared instance across your application.
	   */
	  private static FileDataStoreFactory dataStoreFactory;

	  /** Global instance of the JSON factory. */
	  private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	  /** Global instance of the HTTP transport. */
	  private static HttpTransport httpTransport;

	  private static Storage client;
	  
	  /** Authorizes the installed application to access user's protected data. */
	  private static Credential authorize() throws Exception {
			// load client secrets
			InputStream inputStream = new FileInputStream(CLIENT_SECRET_FILENAME);
			Reader reader = new InputStreamReader(inputStream);
			GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, reader);
					
			// set up authorization code flow
			  GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
			      httpTransport, JSON_FACTORY, clientSecrets,
			      Collections.singleton(StorageScopes.DEVSTORAGE_READ_ONLY)).setDataStoreFactory(
			      dataStoreFactory).build();  
			
			// authorize
			return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
			
		}	  


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();
			dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
			// authorization
		    Credential credential = authorize();
		 // Set up global Storage instance.
		    client = new Storage.Builder(httpTransport, JSON_FACTORY, credential)
		          .setApplicationName(APPLICATION_NAME).build();
		    
		 // Get metadata about the specified bucket.
		      Storage.Buckets.Get getBucket = client.buckets().get(BUCKET_NAME);
		      getBucket.setProjection("full");
		      Bucket bucket = getBucket.execute();
		      System.out.println("name: " + BUCKET_NAME);
		      System.out.println("location: " + bucket.getLocation());
		      System.out.println("timeCreated: " + bucket.getTimeCreated());
		      System.out.println("owner: " + bucket.getOwner());
		      
		   // List the contents of the bucket.
		      Storage.Objects.List listObjects = client.objects().list(BUCKET_NAME);
		      listObjects.setPrefix("NetworkActivity_5851_");
		      com.google.api.services.storage.model.Objects objects;
		      do {
		        objects = listObjects.execute();
		        List<StorageObject> items = objects.getItems();
		        if (null == items) {
		          System.out.println("There were no objects in the given bucket; try adding some and re-running.");
		          break;
		        }
		        for (StorageObject object : items) {
		          System.out.println(object.getName() + " (" + object.getSize() + " bytes)");
		        }
		        listObjects.setPageToken(objects.getNextPageToken());
		      } while (null != objects.getNextPageToken());
		    
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
