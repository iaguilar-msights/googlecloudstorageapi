package com.msights.cloudstorage.main;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.msights.cloudstorage.util.AccessProperties;

public class CopyFilesToOtherAgencyFolders {
	
	private static void copyFiles(String sourceDir, String targetDir){
		File f = new File(sourceDir);
		
		for (File file : f.listFiles()) {
			if (file.isFile()) {
				String fullPath = file.getAbsolutePath();
				String fileName = file.getName();
				//System.out.println("name: " + fullPath);
				//System.out.println("name: " + fileName);
				System.out.println("File to copy: " + fileName);
				try {					
					Files.copy(Paths.get(fullPath), Paths.get(targetDir + fileName), java.nio.file.StandardCopyOption.REPLACE_EXISTING);					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Copy Network files
		String sourceNa = AccessProperties.getNetworkFolder()
												.replace("[Agency]", AccessProperties.getEssenceFolderName())
												.replace("[Region]", "NA");
		
		String sourceEmea = AccessProperties.getNetworkFolder()
												.replace("[Agency]", AccessProperties.getEssenceFolderName())
												.replace("[Region]", "EMEA");										
										
		String targetNa = AccessProperties.getNetworkFolder()
												.replace("[Agency]", AccessProperties.getOMGFolderName())
												.replace("[Region]", "NA");
		
		String targetEmea = AccessProperties.getNetworkFolder()
												.replace("[Agency]", AccessProperties.getOMGFolderName())
												.replace("[Region]", "EMEA");		
		copyFiles(sourceNa, targetNa);
		copyFiles(sourceEmea, targetEmea);
		
		// Copy Matchtable files
		String sourceMatchNa = AccessProperties.getMatchTablesFolder()
				.replace("[Agency]", AccessProperties.getEssenceFolderName())
				.replace("[Region]", "NA");
		
		String sourceMatchEmea = AccessProperties.getMatchTablesFolder()
				.replace("[Agency]", AccessProperties.getEssenceFolderName())
				.replace("[Region]", "EMEA");
		
		String targetMatchNa = AccessProperties.getMatchTablesFolder()
				.replace("[Agency]", AccessProperties.getOMGFolderName())
				.replace("[Region]", "NA");
		
		String targetMatchEmea = AccessProperties.getMatchTablesFolder()
				.replace("[Agency]", AccessProperties.getOMGFolderName())
				.replace("[Region]", "EMEA");
		
		copyFiles(sourceMatchNa, targetMatchNa);
		copyFiles(sourceMatchEmea, targetMatchEmea);
		
	}
	

}
