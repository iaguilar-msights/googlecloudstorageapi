package com.msights.cloudstorage.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.StorageScopes;
import com.google.api.services.storage.model.Bucket;
import com.google.api.services.storage.model.StorageObject;
import com.msights.cloudstorage.util.AccessProperties;
import com.msights.cloudstorage.util.AccessRegionProperties;

public class MainTest {
	
	/**
	   * Be sure to specify the name of your application. If the application name is {@code null} or
	   * blank, the application will log a warning. Suggested format is "MyCompany-ProductName/1.0".
	   * If you are running the sample on a machine where you have access to a browser, set 
	   * AUTH_LOCAL_WEBSERVER to true.
	   */
	  private static final String APPLICATION_NAME = "API_Project";	  
	  private static final String CLIENT_SECRET_FILENAME = "conf/hp_client_secrets.json";
	  
	  /** Global instance of the JSON factory. */
	  private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	  
	  /** Directory to store user credentials. */
	  private static final java.io.File DATA_STORE_DIR = new java.io.File("store/hp_storage_sample");
	  
	  /**
	   * Global instance of the {@link DataStoreFactory}. The best practice is to make it a single
	   * globally shared instance across your application.
	   */
	  private static FileDataStoreFactory dataStoreFactory;	 

	  /** Global instance of the HTTP transport. */
	  private static HttpTransport httpTransport;
	  
	  /** Global instance of the Bucket Name */
	  private static  String bucket_name;

	  /** Global instance of the Storage instance */
	  private static Storage client;
	  
	  
	  
	  /** Authorizes the installed application to access user's protected data. */
	  private static Credential authorize() throws Exception {
			// load client secrets
			InputStream inputStream = new FileInputStream(CLIENT_SECRET_FILENAME);
			Reader reader = new InputStreamReader(inputStream);
			GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, reader);
					
			// set up authorization code flow
			  GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
			      httpTransport, JSON_FACTORY, clientSecrets,
			      Collections.singleton(StorageScopes.DEVSTORAGE_READ_ONLY)).setDataStoreFactory(
			      dataStoreFactory).build();  
			
			// authorize
			return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
			
		}
	  
	  /** This instance is used to request a list of all the objects that matches the specified prefix*/
	  /** Returns a Storage.Objects.List. with the specified prefix*/
	  private static Storage.Objects.List getObjectsListWithPrefix(String prefix) throws IOException{
		  Storage.Objects.List listObjects = client.objects().list(bucket_name);
		    // Set the prefix with the NetworkId and date we need to retrieve
		    listObjects.setPrefix(prefix
		    		.replace("[network_id]", AccessRegionProperties.getNetworkId())
		    		.replace("[date]", AccessProperties.getDownloadDate()));		    		
		    System.out.println("Prefix: " + listObjects.getPrefix());
		    
		    return listObjects;
	  }
	  
	  /** Downloads a Storage Object to the specified folder*/
	  private static void downloadStorageObject(StorageObject object, String folder) {
		  boolean exception = false;
		  do{
			  
			  try{
				  
				  Storage.Objects.Get getObject = client.objects().get(bucket_name, object.getName());
		          // Downloading data.
		          FileOutputStream outputStream =  
		        		  new FileOutputStream(new File(folder
								        				  .replace("[Agency]", AccessProperties.getEssenceFolderName())
								        				  .replace("[Region]", AccessRegionProperties.getRegionName())
								        				  + object.getName()));
		          // If you're not in AppEngine, download the whole thing in one request, if possible.
		          getObject.getMediaHttpDownloader().setDirectDownloadEnabled(true);
		          getObject.executeMediaAndDownloadTo(outputStream);	
		          exception=false;
				  
			  } catch(java.net.SocketException e){
				  System.out.println("Maybe the program stopped running because of a socket exception. ");
				  e.printStackTrace();
				  exception=true;
			  }
			  catch(java.net.SocketTimeoutException e){
				  System.out.println("Maybe the program stopped running  because of a timeout. ");
				  e.printStackTrace();
				  exception=true;
			  }
			  catch(Exception e){
				  System.out.println("Some exception ocurred. ");
				  e.printStackTrace();
				  exception=true;
			  }
			  
		  }
		  while(exception);
		  
		  	
	  }
	  
	  private static ArrayList<StorageObject> downloadNetworkObject(Storage.Objects.List listObjects, String folder) throws IOException{
		  com.google.api.services.storage.model.Objects objects;
		  ArrayList<StorageObject> list = new ArrayList<StorageObject>()   ;
		    do {
		        objects = listObjects.execute();
		        List<StorageObject> items = objects.getItems();
		        if (null == items) {
		          System.out.println("There were no objects in the given bucket; try adding some and re-running.");
		          break;
		        }
		        System.out.println("There are " + items.size() + " Objects in this page that matches the prefix.");
		        for (StorageObject object : items) {
		          System.out.println("File to download: " + object.getName() + " (" + object.getSize() + " bytes)");
		          // Files to be downloaded
		          StorageObject obj = new StorageObject();
		          obj.setName(object.getName());
		          obj.setSize(object.getSize());
		          list.add(obj);
		          // Download object to Network folder
		          downloadStorageObject(object, folder);		          
		        }
		        listObjects.setPageToken(objects.getNextPageToken());
		      } while (null != objects.getNextPageToken());
		    
		    return list;
	  }
	  
	  private static ArrayList<StorageObject> downloadNetworkActivityObject(Storage.Objects.List listObjects, String folder) throws IOException{
		  com.google.api.services.storage.model.Objects objects;
		  ArrayList<StorageObject> list = new ArrayList<StorageObject>()   ;  
		    do {
		        objects = listObjects.execute();
		        List<StorageObject> items = objects.getItems();
		        if (null == items) {
		          System.out.println("There were no objects in the given bucket; try adding some and re-running.");
		          break;
		        }
		        System.out.println("There are " + items.size() + " NetworkActivity Objects in this page.");
		        for (StorageObject object : items) {
		        	
		        	// Since we don't know the spotlight_id, we need to download all the files that matches the download date
		        	if(object.getName().contains(AccessProperties.getDownloadDate()) && !object.getName().contains(".done")){
		        		System.out.println("File to download: " + object.getName() + " (" + object.getSize() + " bytes)");
		        		// Files to be downloaded
				          StorageObject obj = new StorageObject();
				          obj.setName(object.getName());
				          obj.setSize(object.getSize());
				          list.add(obj);
				          // Download object to Network folder
				          downloadStorageObject(object, folder);	
		        	}
		          	          
		        }
		        listObjects.setPageToken(objects.getNextPageToken());
		      } while (null != objects.getNextPageToken());
		    
		    return list;
	  }
	  
	  private static ArrayList<String> getFilesPath(File f){
			ArrayList<String> list =  new ArrayList<String>();
			
			for (File file : f.listFiles()) {
				if (file.isFile()) {
					String name = file.getAbsolutePath();
					list.add(name);
					//System.out.println(name);
				}
			}
				
			return list;
		}
	  
	  private static void checkDownloadedObjects(ArrayList<StorageObject> list, String folder) throws IOException{
		  
		  ArrayList<String> files = getFilesPath(new File(folder
															  .replace("[Agency]", AccessProperties.getEssenceFolderName())
															  .replace("[Region]", AccessRegionProperties.getRegionName())));
		  
		  for(StorageObject o : list){
			if(files.contains(o.getName())){
				
				System.out.println("Retrying to download: " + o.getName());
				downloadStorageObject(o, folder);	
				
			}		
		  }
	  }

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		long timeStart = System.currentTimeMillis();
		
		try {
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();
			dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
			// authorization
		    Credential credential = authorize();
		    // Set up global Storage instance.
		    client = new Storage.Builder(httpTransport, JSON_FACTORY, credential)
		          .setApplicationName(APPLICATION_NAME).build();
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Get the property files so we can access their individual region properties
		 String regions[] = AccessProperties.getPropertyFiles().split(",");
		
		 
		 for(int i = 0; i < regions.length; i++){
			 
			 ArrayList<StorageObject> networkClick = new ArrayList<StorageObject>();
			 ArrayList<StorageObject> networkActivity = new ArrayList<StorageObject>();
			 ArrayList<StorageObject> networkMatchtablesIC = new ArrayList<StorageObject>();
			 ArrayList<StorageObject> networkMatchtablesActivity = new ArrayList<StorageObject>();
			 ArrayList<StorageObject> networkImpression = new ArrayList<StorageObject>();
			 
			 
			 // Set the file to current region so we can access its properties
			 AccessRegionProperties.propertiesFile = regions[i];
			 
			 // Get the name of the bucket we want to access
			 bucket_name = AccessRegionProperties.getBucketName();
				
				try {
														    
				    // Get metadata about the specified bucket.
				    Storage.Buckets.Get getBucket = client.buckets().get(bucket_name);
				    getBucket.setProjection("full");
				    Bucket bucket = getBucket.execute();
				    System.out.println("name: " + bucket_name);
				    System.out.println("location: " + bucket.getLocation());
				    System.out.println("timeCreated: " + bucket.getTimeCreated());
				    System.out.println("owner: " + bucket.getOwner());
				    
				    // Get NetworkClick file
				    System.out.println("Get NetworkClick File");
				    // Build a Storage.Objects.List with the NetworkClick prefix
				    Storage.Objects.List listObjects = getObjectsListWithPrefix(AccessProperties.getNetworkClickPrefix());				  
				    //Download file to Network folder
				    networkClick = downloadNetworkObject(listObjects, AccessProperties.getNetworkFolder());
				    
				    
				    // Get NetworkActivity file
				    System.out.println("Get NetworkActivity File");
				    // Build a Storage.Objects.List with the NetworkActivity prefix
				    listObjects = getObjectsListWithPrefix(AccessProperties.getNetworkActivityPrefix());				  
				    //Download file to Network folder
				    networkActivity = downloadNetworkActivityObject(listObjects, AccessProperties.getNetworkFolder());
				    
				    // Get NetworkMatchtablesIC file
				    System.out.println("Get NetworkMatchtablesIC File");
				    // Build a Storage.Objects.List with the NetworkMatchtablesIC prefix
				    listObjects = getObjectsListWithPrefix(AccessProperties.getNetworkMatchablesICPrefix());				  
				    //Download file to Network folder
				    networkMatchtablesIC = downloadNetworkObject(listObjects, AccessProperties.getMatchTablesFolder());
				    
				    // Get NetworkMatchtablesActivity file
				    System.out.println("Get NetworkMatchtablesActivity File");
				    // Build a Storage.Objects.List with the NetworkMatchtablesActivity prefix
				    listObjects = getObjectsListWithPrefix(AccessProperties.getNetworkMatchablesActivityPrefix());				  
				    //Download file to Network folder
				    networkMatchtablesActivity = downloadNetworkObject(listObjects, AccessProperties.getMatchTablesFolder());
				   
				    
				    // Get NetworkImpression file
				    System.out.println("Get NetworkImpression File");
				    // Build a Storage.Objects.List with the NetworkMatchtablesActivity prefix
				    listObjects = getObjectsListWithPrefix(AccessProperties.getNetworkImpressionPrefix());				  
				    //Download file to Network folder
				    networkImpression = downloadNetworkObject(listObjects, AccessProperties.getNetworkFolder());
				   
				    // Now we check all the files were downloaded		
				    // Download missing files if it is the case
				    System.out.println("Checking if all files were downloaded...");
				    // Download missing NetworkClick files
				    checkDownloadedObjects(networkClick, AccessProperties.getNetworkFolder());
				    
				    // Download missing NetworkActivity files
				    checkDownloadedObjects(networkActivity, AccessProperties.getNetworkFolder());
				    
				    // Download missing NetworkMatchtablesIC files
				    checkDownloadedObjects(networkMatchtablesIC, AccessProperties.getMatchTablesFolder());
				    
				    // Download missing NetworkMatchtablesActivity files
				    checkDownloadedObjects(networkMatchtablesActivity, AccessProperties.getMatchTablesFolder());
				    
				    // Download missing NetworkImpression files
				    checkDownloadedObjects(networkImpression, AccessProperties.getNetworkFolder());
				    
					
				    
				} catch (IOException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				System.out.println();
		 }
		 
		 System.out.println("Total run time: " + (System.currentTimeMillis()-timeStart)/1000/60 + " minutes");

	}

}
