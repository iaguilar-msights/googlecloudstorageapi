package com.msights.cloudstorage.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class AccessRegionProperties {
	
	public static String propertiesFile = "conf/hp_na.properties";
	
	public static String getBucketName(){
		return AccessRegionProperties.getProperty("storage.bucket_name");
	}
	
	public static String getClientSecrets(){
		return AccessRegionProperties.getProperty("storage.client_secrets");
	}
	
	public static String getDataStoreDir(){
		return AccessRegionProperties.getProperty("storage.datastore_dir");
	}
	
	public static String getNetworkId(){
		return AccessRegionProperties.getProperty("storage.network_id");
	}
	
	public static String getRegionName(){
		return AccessRegionProperties.getProperty("storage.region");
	}
	
	
	
	
	private static Properties getProperties() {
		
		InputStream in = null;
		Properties props = null;
		
		try {
			in = new FileInputStream(propertiesFile);
			props = new Properties();
			props.load(in);
		}catch (IOException ie) {	
			System.err.println("ERROR: Found loading properties file.");
			System.err.println(ie.getMessage());
		}finally {
			try {
				if (in != null)
					in.close();
			}catch (IOException ie) {
				ie.printStackTrace();
			}
		}
		
		return props;
	}
			
	public static String getProperty(String key) {
		
		Properties props = AccessRegionProperties.getProperties();
		if (props != null) {
			return props.getProperty(key);
		}
		return null;
	};

}
