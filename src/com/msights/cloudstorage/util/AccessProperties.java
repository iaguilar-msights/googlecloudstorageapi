package com.msights.cloudstorage.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AccessProperties {
	
	public static String propertiesFile = "conf/storage.properties";
	
	public static String getPropertyFiles(){
		return AccessProperties.getProperty("storage.property_files");
	}
	
	public static String getDownloadDate(){
		return AccessProperties.getProperty("storage.download_date");
	}
	
	public static String getNetworkFolder(){
		return AccessProperties.getProperty("storage.network");
	}
	
	public static String getMatchTablesFolder(){
		return AccessProperties.getProperty("storage.matchtables");
	}
	
	public static String getEssenceFolderName(){
		return AccessProperties.getProperty("storage.essence_name");
	}
	
	public static String getOMGFolderName(){
		return AccessProperties.getProperty("storage.omg_name");
	}
	
	public static String getNetworkImpressionPrefix(){
		return AccessProperties.getProperty("storage.network_impression");
	}
	
	public static String getNetworkClickPrefix(){
		return AccessProperties.getProperty("storage.network_click");
	}
	
	public static String getNetworkActivityPrefix(){
		return AccessProperties.getProperty("storage.network_activity");
	}
	
	public static String getNetworkMatchablesICPrefix(){
		return AccessProperties.getProperty("storage.network_match_ic");
	}
	
	public static String getNetworkMatchablesActivityPrefix(){
		return AccessProperties.getProperty("storage.network_match_activity");
	}

	private static Properties getProperties() {
		
		InputStream in = null;
		Properties props = null;
		
		try {
			in = new FileInputStream(propertiesFile);
			props = new Properties();
			props.load(in);
		}catch (IOException ie) {	
			System.err.println("ERROR: Found loading properties file.");
			System.err.println(ie.getMessage());
		}finally {
			try {
				if (in != null)
					in.close();
			}catch (IOException ie) {
				ie.printStackTrace();
			}
		}
		
		return props;
	}
			
	public static String getProperty(String key) {
		
		Properties props = AccessProperties.getProperties();
		if (props != null) {
			return props.getProperty(key);
		}
		return null;
	};
}
