package com.msights.cloudstorage.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

public class DateRangeUpdater {
	
	public static void main(String[] args) {
	    updateDate("conf/storage.properties");
	}

	public static void updateDate(String sDir) {
	    dynamicDateUpdate(1, sDir);
	}
	
	public static void dynamicDateUpdate(int dateOffset1, String sDir) {
	    String downloadDate = dateOffset(dateOffset1);

	    File archive = null;
	    FileReader fr = null;
	    BufferedReader br = null;

	    System.out.println("An automatic Date-Range update will be performed...");
	    try
	    {
	      System.out.println("Updating dates...");
	      archive = new File(sDir);
	      fr = new FileReader(archive);
	      br = new BufferedReader(fr);
	      String newFile = "";
	      String line;
	      while ((line = br.readLine()) != null)
	      {
	        //String line;
	        String newLine = line.replaceAll("storage.download_date=([^<]*)", "storage.download_date=" + downloadDate);
	        newFile = newFile + newLine + "\n";
	      }

	      BufferedWriter bw = new BufferedWriter(new FileWriter(sDir));
	      bw.write(newFile);
	      bw.close();
	      br.close();
	      fr.close();
	      System.out.println("The update has run successfully.");
	    } catch (IOException ex) {
	      System.out.println("An error has been encountered during the upload procedure");
	      System.out.println("Make sure that the directories are correct and revise the 'config' files format");
	      ex.printStackTrace();
	    }
	  }
	
	/**
	 * Returns a String containing the current date minus the offset in days provided.
	 * @param offset
	 * @return
	 */
	private static String dateOffset(int offset) {
		String date;
		String day;
		String month;
		String year;
		// Today's date
		Calendar c = Calendar.getInstance();

		// Subtract the amount of days indicated in the offset
		c.add(Calendar.DATE, -offset);
		day = Integer.toString(c.get(Calendar.DATE));
		// The class calendar assigns 0, 1, 2, ... 11 to the months January, February, ... December
		// We adjust it to 1, 2, 3, ... 12
		month = Integer.toString(c.get(Calendar.MONTH) + 1);
		year = Integer.toString(c.get(Calendar.YEAR));

		if (day.length() == 1) {
			day = "0" + day;
		}
		if (month.length() == 1) {
			month = "0" + month;
		}
		date = month + "-" + day + "-" + year;
		return date;
	}

}
