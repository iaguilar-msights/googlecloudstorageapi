package com.msights.cloudstorage.pojos;

import java.math.BigInteger;

public class StorageObject {
	
	private String name;
	private BigInteger size;
	
	public StorageObject(){
		
	}
	
	public StorageObject(String name, BigInteger size){
		this.name = name;
		this.size = size;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public void setSize(BigInteger size){
		this.size = size;
	}
	
	public BigInteger getSize(){
		return size;
	}
}
